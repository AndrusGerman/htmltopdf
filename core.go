package htmltopdf

import (
	"bytes"
	"html/template"
	"io"

	wkhtmltopdf "github.com/SebastiaanKlippert/go-wkhtmltopdf"
)

// Model html to pdf model
type Model struct {
	Body     *bytes.Buffer
	Dpi      uint
	PageSize string
}

// NewHandler new Model Html to Pdf
func NewHandler() *Model {
	return &Model{
		Body:     new(bytes.Buffer),
		Dpi:      300,
		PageSize: wkhtmltopdf.PageSizeA4,
	}
}

// SetString parsing template function
func (r *Model) SetString(data string) {
	r.Body = nil
	r.Body = bytes.NewBufferString(data)
}

// SetTemplateFile parsing template function
func (r *Model) SetTemplateFile(templateFileName string, data interface{}) error {
	t, err := template.ParseFiles(templateFileName)
	if err != nil {
		return err
	}
	if err = t.Execute(r.Body, data); err != nil {
		return err
	}
	return nil
}

// SetTemplateString parsing template function
func (r *Model) SetTemplateString(templateData string, data interface{}) error {
	t, err := template.New("test").Parse(templateData)
	if err != nil {
		return err
	}
	if err = t.Execute(r.Body, data); err != nil {
		return err
	}
	return nil
}

// GeneratePDFWriter generate pdf function
func (r *Model) GeneratePDFWriter(output io.Writer) error {
	pdfg, err := r.GetWKHTMLTOPDF()
	if err != nil {
		return err
	}
	pdfg.SetOutput(output)
	return pdfg.Create()
}

// GeneratePDFFile generate pdf function
func (r *Model) GeneratePDFFile(name string) error {
	pdfg, err := r.GetWKHTMLTOPDF()
	if err != nil {
		return err
	}
	if err := pdfg.Create(); err != nil {
		return err
	}

	return pdfg.WriteFile(name)
}

// GeneratePDFBuffer generate pdf function
func (r *Model) GeneratePDFBuffer() (*bytes.Buffer, error) {
	pdfg, err := r.GetWKHTMLTOPDF()
	if err != nil {
		return nil, err
	}
	if err := pdfg.Create(); err != nil {
		return nil, err
	}

	return pdfg.Buffer(), nil
}

// GetWKHTMLTOPDF now
func (r *Model) GetWKHTMLTOPDF() (*wkhtmltopdf.PDFGenerator, error) {
	pdfg, err := wkhtmltopdf.NewPDFGenerator()
	if err != nil {
		return nil, err
	}
	pdfg.AddPage(wkhtmltopdf.NewPageReader(r.Body))
	pdfg.PageSize.Set(r.PageSize)
	pdfg.Dpi.Set(r.Dpi)
	return pdfg, nil
}
